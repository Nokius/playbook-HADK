playbook-HADK
===

playbook-HADK shall help starters to get the build enviroment setup easily so they can go on and hack before losing time in setting up the build system.


# Instractions


You need an Ubuntu VM with `git` and `ansable` installed

~~~~
sudo apt install -y git ansible
~~~~

Next checkout this playbook 

~~~~
git clone https://gitlab.com/Nokius/playbook-HADK.git
~~~~ 

Change into the playbook folder

~~~~
cd playbook-HADK/
~~~~

Run the playbook as follwoing (replace `my_target_vendor` with your vendor like lge same for `my_target` set it to your target codename like hammerhead)

~~~~
sudo ansible-playbook -i localhost playbook-hadk.yml --extra-vars "env_user$USER hadk_vendor=my_target_vendor hadk_device=my_target" ; exec bash ; cd $HOME
~~~~

Next step enjoy [HADK Chapter five](https://sailfishos.org/wp-content/uploads/2017/09/SailfishOS-HardwareAdaptationDevelopmentKit-2.0.1.pdf#chapter.5)

To enter the Platform SDK run `sfossdk`

Inside the Platform SDK you can enter the Android Build Environment via `ubu-chroot -r $PLATFORM_SDK_ROOT/sdks/ubuntu`

Happy hacking!
